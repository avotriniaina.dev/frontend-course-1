<div align="center">

<p><img src="https://static.packt-cdn.com/products/9781838648121/graphics/assets/9a5e3a54-0f0e-42a2-ab09-3ab748173cfe.png" width="300"></p>

# Développeur Web Front-End
### Je vous souhaite la bienvenue à cette première leçon.

#### Voir sur:

[![build status](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRchsUMxwc-0oWzfYdCMZj8PDywh_y9MTqczKDD4XLN8lIfVfErJOgmrAj4Bq3pNXL0iA&usqp=CAU)](https://github.com/avotriniaina/)

</div>

----

## A propos de moi

[![for_the_badge](https://logodix.com/logo/634368.gif)](https://monitor.f-droid.org/builds)

> Portfolio : `https://avotriniaina.rf.gd'


## Structure

>> 1. Top Menu
>> 2. Home

<!-- ## More information

You can find more details in the [documentation](https://f-droid.org/docs).


## Donate

[![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/F-Droid-Data)


## Contributing

See the [Contributing](CONTRIBUTING.md) documentation.


## Translation

Many app summaries and some descriptions can be translated as part of F-Droid. See [Translation and Localization](https://f-droid.org/docs/Translation_and_Localization)
for more info.

<div align="center">

[![](https://hosted.weblate.org/widgets/f-droid/-/287x66-white.png)](https://hosted.weblate.org/engage/f-droid)

<details>
<summary>View translation status for all languages.</summary>

[![translation status](https://hosted.weblate.org/widgets/f-droid/-/fdroiddata/multi-auto.svg)](https://hosted.weblate.org/engage/f-droid/?utm_source=widget)

</details> -->

</div>
